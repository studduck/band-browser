<?php

namespace Ac\BandBrowser\Tests\Application\Controller;

use Ac\BandBrowser\Application\Controller\ArtistController;

class ArtistControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testIndexActionForCountry()
    {
        $response = 'response';

        $api = $this->getMockBuilder('Ac\BandBrowser\Application\Database\GetData')
            ->disableOriginalConstructor()
            ->getMock();

        $api->method('getTopArtists')
            ->willReturn($response);

        $indexController = new ArtistController($api);

        $result = $indexController->indexAction(
            null,
            'spain'
        );

        $this->assertEquals(
            $result->getContent(),
            $response
        );
    }

    public function testIndexActionForArtist()
    {
        $response = 'response';

        $api = $this->getMockBuilder('Ac\BandBrowser\Application\Database\GetData')
            ->disableOriginalConstructor()
            ->getMock();

        $api->method('getArtist')
            ->willReturn($response);

        $indexController = new ArtistController($api);

        $result = $indexController->indexAction(
            'tool'
        );

        $this->assertEquals(
            $result->getContent(),
            $response
        );
    }

    public function testTrackAction()
    {
        $response = 'response';

        $api = $this->getMockBuilder('Ac\BandBrowser\Application\Database\GetData')
            ->disableOriginalConstructor()
            ->getMock();

        $api->method('getTopTracks')
            ->willReturn($response);

        $indexController = new ArtistController($api);

        $result = $indexController->tracksAction(
            'tool'
        );

        $this->assertEquals(
            $result->getContent(),
            $response
        );
    }
}
