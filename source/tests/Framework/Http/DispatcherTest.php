<?php

namespace Ac\BandBrowser\Tests\Framework\Http;

use Ac\BandBrowser\Framework\Http\Dispatcher;

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    public function testDispatch()
    {
        $controller = 'artist';
        $action = 'index';
        $arg = 'tool';
        $response = 'response';

        $uri = sprintf(
            '/%s/%s',
            $controller,
            $arg
        );

        $indexController = $this->getMockBuilder('Ac\BandBrowser\Application\Controller\ArtistController')
            ->disableOriginalConstructor()
            ->getMock();

        $indexController->method(sprintf('%sAction', $action))
            ->with($arg)
            ->willReturn($response);

        $controllerFactory = $this->getMockBuilder('\Ac\BandBrowser\Framework\Controller\Factory')
            ->disableOriginalConstructor()
            ->getMock();

        $controllerFactory->method('build')
            ->with()
            ->willReturn($indexController);

        $dispatcher = new Dispatcher($uri, [], $controllerFactory);

        $result = $dispatcher->dispatch();

        $this->assertEquals($response, $result);
    }
}
