<?php

namespace Ac\BandBrowser\Tests\Framework\Http;

use Ac\BandBrowser\Framework\Http\Router;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    public function testGenerateForHomePage()
    {
        $expected = [
            Router::ROUTE_KEY => [
                Router::CONTROLLER_KEY => Router::INDEX_KEY,
                Router::ACTION_KEY => Router::INDEX_KEY
            ],
            Router::ARGS_KEY => []
        ];

        $result = Router::generate('/');

        $this->assertEquals($expected, $result);
    }

    public function testGenerateForIndexOfController()
    {
        $controllerPath = 'artist';

        $expected = [
            Router::ROUTE_KEY => [
                Router::CONTROLLER_KEY => $controllerPath,
                Router::ACTION_KEY => Router::INDEX_KEY
            ],
            Router::ARGS_KEY => []
        ];

        $result = Router::generate(sprintf('/%s', $controllerPath));

        $this->assertEquals($expected, $result);
    }

    public function testGenerateForIndexOfControllerWithArgs()
    {
        $controllerPath = 'artist';
        $arg = 'tool';

        $expected = [
            Router::ROUTE_KEY => [
                Router::CONTROLLER_KEY => $controllerPath,
                Router::ACTION_KEY => Router::INDEX_KEY
            ],
            Router::ARGS_KEY => [$arg]
        ];

        $result = Router::generate(
            sprintf(
                '/%s/%s',
                $controllerPath,
                $arg
            )
        );

        $this->assertEquals($expected, $result);
    }

    public function testGenerateForActionOfControllerWithArgs()
    {
        $controllerPath = 'artist';
        $actionPath = 'create';
        $arg = 'tool';

        $expected = [
            Router::ROUTE_KEY => [
                Router::CONTROLLER_KEY => $controllerPath,
                Router::ACTION_KEY => $actionPath
            ],
            Router::ARGS_KEY => [$arg]
        ];

        $result = Router::generate(
            sprintf(
                '/%s/%s/%s',
                $controllerPath,
                $actionPath,
                $arg
            )
        );

        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException \Exception
     */
    public function testInvalidPathThrowsError()
    {
        Router::generate('/this/path/is/way/too/long');
    }
}
