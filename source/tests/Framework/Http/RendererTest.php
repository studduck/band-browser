<?php

namespace Ac\BandBrowser\Framework\Http;

use Ac\BandBrowser\Framework\Http\Renderer;

function http_response_code($code)
{
    return $code;
}

function header($header)
{
    return $header;
}

/**
 * @group current
 */
class RendererTest extends \PHPUnit_Framework_TestCase
{
    public function testRender()
    {
        $responseCode = 200;
        $contentType = 'application/json';

        $response = $this->getMockBuilder('Ac\BandBrowser\Framework\Http\Response')
            ->disableOriginalConstructor()
            ->getMock();

        $response->method('getStatusCode')
            ->willReturn($responseCode);

        $response->method('getContentType')
            ->willReturn($contentType);

        $renderer = new Renderer();

        $renderer->render($response);
    }
}
