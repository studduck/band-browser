<?php

namespace Ac\BandBrowser\Tests\Framework\Config;

use Ac\BandBrowser\Framework\Config\ServiceConfigBuilder;

class ServiceConfigBuilderTest extends \PHPUnit_Framework_TestCase
{
    public function testBuildServiceConfig()
    {
        $parentService = '\Test\ParentService';
        $childService = '\Test\ChildService';
        $childConfigKey = 'childConfig';
        $childConfig = ['url' => 'http://test.com'];

        $services = [
            $parentService => [
                $childService => $childConfigKey
            ]
        ];

        $config = [
            $childConfigKey => $childConfig
        ];

        $expected = $services;
        $expected[$parentService][$childService] = $childConfig;

        $result = ServiceConfigBuilder::buildServiceConfig($services, $config);

        $this->assertEquals($expected, $result);
    }

    /**
     * @expectedException \Exception
     */
    public function testBuildServiceWithNoConfig()
    {
        $parentService = '\Test\ParentService';
        $childService = '\Test\ChildService';
        $childConfigKey = 'childConfig';
        $config = [];

        $services = [
            $parentService => [
                $childService => $childConfigKey
            ]
        ];

        $result = ServiceConfigBuilder::buildServiceConfig($services, $config);
    }
}
