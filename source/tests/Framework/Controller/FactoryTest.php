<?php

namespace Ac\BandBrowser\Tests\Framework\Controller;

use Ac\BandBrowser\Framework\Controller\Factory;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    protected $serviceFactory;

    public function setup()
    {
        $this->serviceFactory = $this->getMockBuilder('Ac\BandBrowser\Framework\Service\Factory')
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testBuild()
    {
        $controller = 'artist';
        $namespace = 'Ac\BandBrowser';
        $controllerPath = sprintf(
            '%s\Application\Controller\%sController',
            $namespace,
            ucfirst(strtolower($controller))
        );

        $api = $this->getMockBuilder('Ac\BandBrowser\Application\Database\GetData')
            ->disableOriginalConstructor()
            ->getMock();

        $this->serviceFactory->method('build')
            ->with($controllerPath)
            ->willReturn([$api]);

        $factory = new Factory($namespace, $this->serviceFactory);

        $result = $factory->build($controller);

        $this->assertEquals(
            get_class($result),
            'Ac\BandBrowser\Application\Controller\ArtistController'
        );
    }

    /**
     * @expectedException \Exception
     */
    public function testBuildForInvalidController()
    {
        $controller = 'does-not-exist';
        $namespace = 'Ac\BandBrowser';

        $factory = new Factory($namespace, $this->serviceFactory);

        $result = $factory->build($controller);
    }
}
