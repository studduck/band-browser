<?php

namespace Ac\BandBrowser\Tests\Framework;

use Ac\BandBrowser\Framework\Application;

class ApplicationTest extends \PHPUnit_Framework_TestCase
{
    public function testRun()
    {
        $response = $this->getMockBuilder('Ac\BandBrowser\Framework\Http\Response')
            ->disableOriginalConstructor()
            ->getMock();

        $dispatcher = $this->getMockBuilder('Ac\BandBrowser\Framework\Http\Dispatcher')
            ->disableOriginalConstructor()
            ->getMock();

        $dispatcher->method('dispatch')
            ->willReturn($response);

        $renderer = $this->getMockBuilder('Ac\BandBrowser\Framework\Http\Renderer')
            ->disableOriginalConstructor()
            ->getMock();

        $renderer->method('render')
            ->with($response);

        $application = new Application($dispatcher, $renderer);

        $application->run();
    }
}
