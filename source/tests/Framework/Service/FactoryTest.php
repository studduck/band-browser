<?php

namespace Ac\BandBrowser\Tests\Framework\Service;

use Ac\BandBrowser\Framework\Service\Factory;

class FactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testBuild()
    {
        $classPath = '\stdClass';
        $params = [1, 2];

        $config = [
            $classPath => [$classPath => $params]
        ];

        $factory = new Factory($config);

        $result = $factory->build($classPath);

        $expected = [new \stdClass()];

        $this->assertEquals($expected, $result);
    }
}
