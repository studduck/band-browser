<?php

namespace Ac\BandBrowser\Application\Database;

use Ac\BandBrowser\Application\Database\Api;

function file_get_contents($path)
{
    return $path;
}

class ApiTest extends \PHPUnit_Framework_TestCase
{
    protected $getTopArtistsMethod;
    protected $getArtistMethod;
    protected $getTopTracksMethod;
    protected $endpoint;
    protected $version;
    protected $apiKey;
    protected $api;

    public function setup()
    {
        $this->getTopArtistsMethod = 'get.all';
        $this->getArtistMethod = 'get';
        $this->getTopTracksMethod = 'get.tracks';
        $this->endpoint = 'test';
        $this->version = '1.0';
        $this->apiKey = 'asc312';

        $this->api = new Api(
            $this->getTopArtistsMethod,
            $this->getArtistMethod,
            $this->getTopTracksMethod,
            $this->endpoint,
            $this->version,
            $this->apiKey
        );
    }

    public function testGetTopArtists()
    {
        $country = 'spain';
        $limit = 5;
        $page = 1;

        $expected = sprintf(
            '%s/%s/?api_key=%s&format=json&method=%s&country=%s&limit=%d&page=%d',
            $this->endpoint,
            $this->version,
            $this->apiKey,
            $this->getTopArtistsMethod,
            $country,
            $limit,
            $page
        );

        $result = $this->api->getTopArtists($country, $limit, $page);

        $this->assertEquals($result, $expected);
    }

    public function testGetArtist()
    {
        $artist = 'tool';

        $expected = sprintf(
            '%s/%s/?api_key=%s&format=json&method=%s&artist=%s',
            $this->endpoint,
            $this->version,
            $this->apiKey,
            $this->getArtistMethod,
            $artist
        );

        $result = $this->api->getArtist($artist);

        $this->assertEquals($result, $expected);
    }

    public function testGetTopTracks()
    {
        $artist = 'tool';
        $limit = 1;
        $page = 5;

        $expected = sprintf(
            '%s/%s/?api_key=%s&format=json&method=%s&artist=%s&limit=%d&page=%d',
            $this->endpoint,
            $this->version,
            $this->apiKey,
            $this->getTopTracksMethod,
            $artist,
            $limit,
            $page
        );

        $result = $this->api->getTopTracks($artist, $limit, $page);

        $this->assertEquals($result, $expected);
    }
}
