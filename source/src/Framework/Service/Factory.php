<?php

namespace Ac\BandBrowser\Framework\Service;

class Factory
{
    protected $serviceConfig;

    public function __construct(array $serviceConfig)
    {
        $this->serviceConfig = $serviceConfig;
    }

    /**
     * Build any services required for
     * a configured class
     * @param string $classPath
     * @return array
     */
    public function build($classPath)
    {
        $services = [];

        if (isset($this->serviceConfig[$classPath])) {
            foreach ($this->serviceConfig[$classPath] as $class => $params) {
                $services[] = new $class(...array_values($params));
            }
        }

        return $services;
    }
}
