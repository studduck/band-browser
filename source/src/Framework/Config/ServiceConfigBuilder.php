<?php

namespace Ac\BandBrowser\Framework\Config;

class ServiceConfigBuilder
{
    /**
     * Takes a service definition array and
     * pulls any required configuration from
     * the application config
     * @param array $services
     * @param array $appConfig
     * @return array
     */
    public static function buildServiceConfig(array $services, array $appConfig)
    {
        foreach ($services as $class => &$serviceDependencies) {
            foreach ($serviceDependencies as $serviceClass => $serviceConfig) {
                if (!isset($appConfig[$serviceConfig])) {
                    throw new \Exception('Config is not set for service: '.$serviceClass);
                }

                $serviceDependencies[$serviceClass] = $appConfig[$serviceConfig];
            }
        }

        return $services;
    }
}
