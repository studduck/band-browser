<?php

namespace Ac\BandBrowser\Framework;

use Ac\BandBrowser\Framework\Http\Dispatcher;
use Ac\BandBrowser\Framework\Http\Renderer;
use Ac\BandBrowser\Framework\Http\Response;
use Ac\BandBrowser\Framework\Http\Codes;

class Application
{
    protected $dispatcher;

    public function __construct(Dispatcher $dispatcher, Renderer $renderer)
    {
        $this->dispatcher = $dispatcher;
        $this->renderer = $renderer;
    }

    /**
     * Gets a response from the dispatcher
     * and then renders it.
     * Will catch any not found exceptions
     * and return a 404.
     * @return string Response content
     */
    public function run()
    {
        try {
            $response = $this->dispatcher->dispatch();
        } catch (\Exception $e) {
            $interfaces = class_implements($e);

            if (isset($interfaces['Ac\BandBrowser\Framework\Controller\NotFoundException'])) {
                $code = CODES::NOT_FOUND;
            } else {
                $code = CODES::BAD_REQUEST;
            }

            $response = new Response(
                $code,
                $e->getMessage()
            );
        }

        $this->renderer->render($response);
    }
}
