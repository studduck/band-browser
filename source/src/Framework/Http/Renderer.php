<?php

namespace Ac\BandBrowser\Framework\Http;

class Renderer
{
    protected $cache;

    public function __construct($cache = 600)
    {
        $this->cache = $cache;
    }

    /**
     * Render a response object
     * @param Response $response
     */
    public function render(Response $response)
    {
        http_response_code($response->getStatusCode());

        header(
            sprintf('Content-Type: %s; charset=utf-8', $response->getContentType())
        );

        header(
            sprintf("Cache-Control: max-age=%d", $this->cache)
        );

        echo $response->getContent();
    }
}
