<?php

namespace Ac\BandBrowser\Framework\Http;

class Router
{
    const ROUTE_KEY = 'route';
    const ARGS_KEY = 'args';
    const CONTROLLER_KEY = 'controller';
    const ACTION_KEY = 'action';
    const INDEX_KEY = 'index';

    /**
     * Generates route information from the given uri
     * @param string $uri The current requests uri
     * @return array('route' => ['controller', 'action'], args => ['args'])
     */
    public static function generate($uri)
    {
        $path = self::parseUri($uri);
        return self::routePath($path);
    }

    /**
     * Parses the current uri and returns an array
     * representing it's path
     * @param string $uri
     * @return array
     */
    public static function parseUri($uri)
    {
        $path = trim(
            parse_url(
                $uri,
                PHP_URL_PATH
            ),
            "/"
        );

        return explode('/', $path);
    }

    /**
     * Routes the path to a specific controller
     * and action with any arguments
     * @param array(string, string) $path
     * @return array('route' => ['controller', 'action'], args => ['args'])
     * @throws \Exeception Throws an exception if path is too long
     */
    public static function routePath($path)
    {
        $pathCount = count($path);

        if ($pathCount && $pathCount <= 3) {
            $route = [];
            $args = [];

            if ($pathCount === 1) {
                $route[self::CONTROLLER_KEY] = !empty($path[0]) ? $path[0] : self::INDEX_KEY;
                $route[self::ACTION_KEY] = self::INDEX_KEY;
            } else {
                $args = $path;

                $route[self::CONTROLLER_KEY] = $path[0];
                unset($args[0]);
                if ($pathCount === 2) {
                    $route[self::ACTION_KEY] = self::INDEX_KEY;
                } else {
                    $route[self::ACTION_KEY] = $path[1];
                    unset($args[1]);
                }
            }

            return [
                self::ROUTE_KEY => $route,
                self::ARGS_KEY => array_values($args)
            ];
        }

        throw new \Exception('Path could not be routed.');
    }
}
