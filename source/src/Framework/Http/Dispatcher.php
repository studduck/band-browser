<?php

namespace Ac\BandBrowser\Framework\Http;

use Ac\BandBrowser\Framework\Controller\Factory;
use Ac\BandBrowser\Framework\Controller\ActionNotFoundException;
use Ac\BandBrowser\Framework\Controller\ParameterNotFoundException;
use Ac\BandBrowser\Framework\Http\Controller;

class Dispatcher
{
    protected $uri;
    protected $queryParams;
    protected $controllerFactory;

    public function __construct(
        $uri,
        array $queryParams,
        Factory $controllerFactory
    ) {
        $this->uri = $uri;
        $this->queryParams = $queryParams;
        $this->controllerFactory = $controllerFactory;
    }

    /**
     * Takes a uri and dispatches to the required
     * controller and action and returns the response
     * @return Ac\BandBrowser\Http\Response
     */
    public function dispatch()
    {
        $routeInfo = Router::generate($this->uri);

        $controllerPath = $routeInfo[Router::ROUTE_KEY][Router::CONTROLLER_KEY];
        $action = $routeInfo[Router::ROUTE_KEY][Router::ACTION_KEY];
        $args = array_merge($routeInfo[Router::ARGS_KEY], $this->queryParams);

        $controller = $this->controllerFactory->build($controllerPath);
        $action = sprintf('%sAction', $action);

        $requiredArgs = $this->buildArgsFromQueryParams($args, $controller, $action);

        return $controller->$action(...$requiredArgs);
    }

    /**
     * Merge arguments with any query params
     * for the required controller object
     * @param array $args
     * @param Controller $controller
     * @param string $action
     * @return array
     */
    public function buildArgsFromQueryParams(array $args, Controller $controller, $action)
    {
        $requiredArgs = [];
        $reflectionClass = new \ReflectionClass($controller);

        try {
            $method = $reflectionClass->getMethod($action);
        } catch (\Exception $e) {
            throw new ActionNotFoundException(
                sprintf(
                    '%s not found in %s',
                    $action,
                    get_class($controller)
                )
            );
        }

        $params = $method->getParameters();

        foreach ($params as $pos => $param) {
            $paramName = $param->name;
            if (isset($args[$paramName])) {
                $requiredArgs[$pos] = $args[$paramName];
            } elseif (isset($args[$pos])) {
                $requiredArgs[$pos] = $args[$pos];
            } elseif (!$param->isOptional()) {
                throw new ParameterNotFoundException(
                    sprintf(
                        '%s parameter is required by %s:%s',
                        $paramName,
                        get_class($controller),
                        $action
                    )
                );
            } else {
                $requiredArgs[$pos] = $param->getDefaultValue();
            }
        }

        return $requiredArgs;
    }
}
