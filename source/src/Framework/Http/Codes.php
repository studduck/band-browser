<?php

namespace Ac\BandBrowser\Framework\Http;

/**
 * @codeCoverageIgnore
 */
class Codes
{
    const OK = 200;
    const BAD_REQUEST = 400;
    const NOT_FOUND = 404;

    private function __construct()
    {
        throw new \Exception("Can't get an instance of Codes");
    }
}
