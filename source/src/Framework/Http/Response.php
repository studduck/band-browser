<?php

namespace Ac\BandBrowser\Framework\Http;

class Response
{
    protected $statusCode;
    protected $content;
    protected $contentType;

    public function __construct($statusCode, $content, $contentType = 'application/json')
    {
        $this->statusCode = $statusCode;
        $this->content = $content;
        $this->contentType = $contentType;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getContentType()
    {
        return $this->contentType;
    }
}
