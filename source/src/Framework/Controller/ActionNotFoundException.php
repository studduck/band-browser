<?php

namespace Ac\BandBrowser\Framework\Controller;

class ActionNotFoundException extends \Exception implements NotFoundException
{
}
