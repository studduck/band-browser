<?php

namespace Ac\BandBrowser\Framework\Controller;

use Ac\BandBrowser\Framework\Service\Factory as ServiceFactory;

class Factory
{
    protected $namespace;
    protected $serviceFactory;

    public function __construct($namespace, ServiceFactory $serviceFactory)
    {
        $this->namespace = $namespace;
        $this->serviceFactory = $serviceFactory;
    }

    /**
     * Builds a controller object
     * @param string $controller
     * @return Ac\BandBrowser\Controller\{$controller}Controller
     */
    public function build($controller)
    {
        $controllerPath = sprintf(
            '%s\Application\Controller\%sController',
            $this->namespace,
            ucfirst(strtolower($controller))
        );

        if (!class_exists($controllerPath)) {
            throw new ControllerNotFoundException(
                sprintf('%s does not exist.', $controllerPath)
            );
        }

        $services = $this->serviceFactory->build($controllerPath);

        return new $controllerPath(...$services);
    }
}
