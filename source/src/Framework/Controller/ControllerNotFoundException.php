<?php

namespace Ac\BandBrowser\Framework\Controller;

class ControllerNotFoundException extends \Exception implements NotFoundException
{
}
