<?php

namespace Ac\BandBrowser\Application\Controller;

interface HttpResponseBuilder
{
    /**
     * Build a successful response object
     * @param array $responseData
     * @return Response
     */
    public function buildResponse($responseData);
}
