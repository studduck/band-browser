<?php

namespace Ac\BandBrowser\Application\Controller;

class ArtistController extends GetController
{
    /**
     * The index action for artists
     * Will return artists by country or a
     * single artist by name
     * @param string|null $artist
     * @param string|null $country
     * @param int $limit
     * @param int $page
     * @return array
     */
    public function indexAction($artist = null, $country = null, $limit = 5, $page = 1)
    {
        if (!empty($artist)) {
            $results = $this->getApi()->getArtist($artist);
        } elseif (!empty($country)) {
            $results = $this->getApi()->getTopArtists($country, $limit, $page);
        } else {
            throw new \Exception('Artist of country required to pull artists');
        }

        return $this->buildResponse($results);
    }

    /**
     * Will return the top tracks for an artist
     * @param string $artist
     * @param int $limit
     * @param int $page
     * @return array
     */
    public function tracksAction($artist, $limit = 5, $page = 1)
    {
        $results = $this->getApi()->getTopTracks($artist, $limit, $page);

        return $this->buildResponse($results);
    }
}
