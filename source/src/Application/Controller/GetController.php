<?php

namespace Ac\BandBrowser\Application\Controller;

use Ac\BandBrowser\Application\Database\GetData;
use Ac\BandBrowser\Framework\Http\Controller;
use Ac\BandBrowser\Framework\Http\Response;
use Ac\BandBrowser\Framework\Http\Codes;

abstract class GetController implements HttpResponseBuilder, Controller
{
    protected $api;

    public function __construct(GetData $api)
    {
        $this->api = $api;
    }

    /**
     * Return the api client
     * @return GetData
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * {@inheritdoc}
     */
    public function buildResponse($responseData)
    {
        $response = new Response(
            Codes::OK,
            $responseData
        );

        return $response;
    }
}
