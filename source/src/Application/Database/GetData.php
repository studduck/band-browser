<?php

namespace Ac\BandBrowser\Application\Database;

interface GetData
{
    /**
     * Return the top artists by country
     * @param string $country
     * @param int $limit
     * @param int $page
     * @return string
     */
    public function getTopArtists($country, $limit, $page);

    /**
     * Get an artist by name
     * @param string $artist
     * @return string
     */
    public function getArtist($artist);

    /**
     * Get top tracks for an artist
     * @param string $artist
     * @param int $limit
     * @param int $page
     * @return string
     */
    public function getTopTracks($artist, $limit, $page);
}
