<?php

namespace Ac\BandBrowser\Application\Database;

class Api implements GetData
{
    protected $endpoint;

    public function __construct(
        $getTopArtistsMethod,
        $getArtistMethod,
        $getTopTracksMethod,
        $url,
        $version,
        $key,
        $format = 'json'
    ) {
        $this->getTopArtistsMethod = $getTopArtistsMethod;
        $this->getArtistMethod = $getArtistMethod;
        $this->getTopTracksMethod = $getTopTracksMethod;
        $this->endpoint = sprintf(
            '%s/%s/?api_key=%s&format=%s',
            $url,
            $version,
            $key,
            $format
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getTopArtists($country, $limit, $page)
    {
        $endpoint = sprintf(
            '%s&method=%s&country=%s&limit=%d&page=%d',
            $this->endpoint,
            $this->getTopArtistsMethod,
            $country,
            $limit,
            $page
        );

        return $this->getFromApi($endpoint);
    }

    /**
     * {@inheritdoc}
     */
    public function getArtist($artist)
    {
        $endpoint = sprintf(
            '%s&method=%s&artist=%s',
            $this->endpoint,
            $this->getArtistMethod,
            $artist
        );

        return $this->getFromApi($endpoint);
    }

    /**
     * {@inheritdoc}
     */
    public function getTopTracks($artist, $limit, $page)
    {
        $endpoint = sprintf(
            '%s&method=%s&artist=%s&limit=%d&page=%d',
            $this->endpoint,
            $this->getTopTracksMethod,
            $artist,
            $limit,
            $page
        );

        return $this->getFromApi($endpoint);
    }

    /**
     * Get data from the api endpoint
     * @param string $endpoint
     * @return string
     */
    protected function getFromApi($endpoint)
    {
        $response = file_get_contents($endpoint);

        return $response;
    }
}
