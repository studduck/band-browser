'use strict';

angular.module('BandBrowser', [])
  .controller('BandController', function($scope, $http){
    $scope.$watch('search', function() {
      $scope.page = 1;
      searchByCountry();
    });

    $scope.previous = function() {
      $scope.page--;
      searchByCountry();
    };

    $scope.next = function() {
      $scope.page++;
      searchByCountry();
    };

    $scope.setArtist = function(artistCount) {
      var artist = $scope.artistList[artistCount];
      getArtistInfo(artist);
    }

    $scope.resetArtist = function() {
      $scope.artist = undefined;
    }

    $scope.page = 1;
    $scope.limit = 5;

    function resetForm(){
      $scope.artistList = null;
      $scope.page = 1;
    }

    function getArtistInfo(artist)
    {
      $http.get("/api/artist/tracks/" + artist.name)
        .then(function(response){
          if(response.data.toptracks &&
            response.data.toptracks.track) {
            artist.tracks = response.data.toptracks.track;
            $scope.artist = artist;
          }
      });

    }

    function searchByCountry(){
      if($scope.search) {
        $http.get("/api/artist?country=" + $scope.search + "&page=" + $scope.page)
          .then(function(response){
            if (!response.data.topartists ||
              !response.data.topartists.artist ||
              !response.data.topartists.artist.length > 0
              ) {
              resetForm();
            } else {
              // The lastfm api is sometimes returning much more elements
              // than the limit. Need to make sure we show the last 5.
              var artistList = response.data.topartists.artist;
              $scope.artistList = artistList.slice(artistList.length-5, artistList.length);
            }
         })
      } else {
        resetForm();
      }
    }
  });
