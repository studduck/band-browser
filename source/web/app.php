<?php
namespace Ac\BandBrowser;

use Ac\BandBrowser\Framework\Application;
use Ac\BandBrowser\Framework\Config\ServiceConfigBuilder;
use Ac\BandBrowser\Framework\Http\Dispatcher;
use Ac\BandBrowser\Framework\Http\Renderer;
use Ac\BandBrowser\Framework\Controller\Factory as ControllerFactory;
use Ac\BandBrowser\Framework\Service\Factory as ServiceFactory;

error_reporting(E_ALL);
ini_set('display_errors', 1);

$namespace = __NAMESPACE__;

require_once '../autoload.php';
$config = require_once '../config/config.php';
$services = require_once '../config/services.php';

$serviceConfig = ServiceConfigBuilder::buildServiceConfig($services, $config);

$uri = str_replace('/api', '', $_SERVER['REQUEST_URI']);

$dispatcher = new Dispatcher(
    strtolower($uri),
    $_GET,
    new ControllerFactory(
        $namespace,
        new ServiceFactory($serviceConfig)
    )
);

$renderer = new Renderer();

$application = new Application($dispatcher, $renderer);
$response = $application->run();

echo $response;
