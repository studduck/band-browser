#! /bin/bash

# System
apt-get install -y git acl ruby2.0
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install pyopenssl ndg-httpsclient pyasn1 awscli

# PHP 5
LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
apt-get update
apt-get install -y php5.6 php5.6-fpm

rm -f /etc/php/5.6/fpm/pool.d/*
aws s3 cp s3://band-browser-util/fpm/bb.conf /etc/php/5.6/fpm/pool.d/bb.conf --region ap-southeast-2
service php5.6-fpm restart

# Nginx
apt-get install nginx -y
rm -f /etc/nginx/sites-enabled/*
aws s3 cp s3://band-browser-util/nginx/band-browser.joshuacullen.com.conf /etc/nginx/sites-available/band-browser.joshuacullen.com.conf --region ap-southeast-2
ln -s /etc/nginx/sites-available/band-browser.joshuacullen.com.conf /etc/nginx/sites-enabled/band-browser.joshuacullen.com.conf
service nginx reload

# Project and permissions
DEPLOY_USER="deployer"
WEB_SERVER_USER="www-data"
PROJECT_ROOT="/var/projects"

id -u $DEPLOY_USER &>/dev/null || useradd -m $DEPLOY_USER
mkdir -p $PROJECT_ROOT
chown $DEPLOY_USER:$DEPLOY_USER $PROJECT_ROOT

setfacl -R -m u:$WEB_SERVER_USER:rwX -m u:$DEPLOY_USER:rwX $PROJECT_ROOT
setfacl -dR -m u:$WEB_SERVER_USER:rwX -m u:$DEPLOY_USER:rwX $PROJECT_ROOT

# Codedeploy
cd /home/ubuntu
wget https://aws-codedeploy-ap-southeast-2.s3.amazonaws.com/latest/install
chmod +x ./install
./install auto
