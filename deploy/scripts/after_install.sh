#!/bin/bash

DEPLOYER_USER="deployer"
PROJECT_ROOT="/var/projects"

aws s3 cp s3://band-browser-util/config.php $PROJECT_ROOT/bb/config/ --region ap-southeast-2
chown -R $DEPLOYER_USER:$DEPLOYER_USER $PROJECT_ROOT
